Samo "npm start".

* U slucaju da pukne i ne pokrene automatski http-server, isti otvoriti u "app" folderu gde je index.html
* U slucaju potrebe za editovanjem, ima gulp pa se editovanje nece primeniti ukoliko nije pokrenut gulp watcher, pokrenuti sa "gulp" komandom.
* Na iphone-u ne radi ako se responsive testiranje radi preko localhost-a, proradi kad se postavi na validan server. 
  (konkretno window.navigator.geolocation.getCurrentPosition funkcija zahteva validan http, Apple-ovo pravilo )