var app = angular.module('app', ['ui.router']);

app.run(function($rootScope) {
 
});

app.filter('round', function() {
    return function(input) {
      return Math.floor(input);
    }
});


// Application Level State
app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.when('', '/main');

  $stateProvider
    .state('app', {
      url: '',
      controller: 'AppCtrl',
      views: {
        'controls': {
          templateUrl: 'js/templates/controls.html',
          controller: 'ControlsCtrl'
        },
        'main': {
          templateUrl: 'js/templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })
    .state('app.main', {
      url: '/main',
      templateUrl: 'js/templates/main.html',
      controller: 'MainCtrl'
    })
    .state('404', {
      url: '/404',
      templateUrl: 'js/templates/404.html',
      controller: 'AppCtrl'
    });

}]);
app.controller('AppCtrl', ['$scope',
							function($scope) {

}]);
app.controller('ControlsCtrl', ['$scope',
							function($scope) {

}]);
app.controller('MainCtrl', ['$scope',
	'GetLocation',
	'ConfigService',
	function ($scope, GetLocation, ConfigService) {

	$scope.propertyName = 'distance';
	$scope.reverse = true;
	$scope.sortBy = function(propertyName) {
		//console.log(propertyName)
	  $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	  $scope.propertyName = propertyName;
	};

	$scope.map = new google.maps.Map(document.getElementById('map'));
	var infowindow = new google.maps.InfoWindow();
	var service = new google.maps.places.PlacesService($scope.map);
	var directionsService = new google.maps.DirectionsService;
	$scope.atmList = [];
	var currentLocation = {};

	$scope.showModal = false;

	$scope.getAtm = function(atm) {
		$( ".map-flex-container" ).addClass('map-blur')
		$scope.showModal = true;
	    console.log(atm)
		$scope.mapAttr = atm;
	}

	$scope.closeModal = function (a) {
		$scope.showModal = false;
		$( ".map-flex-container" ).removeClass('map-blur')
	}



	GetLocation.locate().then(function (a) {

		console.log(a)

		currentLocation =  { lat: a.coords.latitude, lng: a.coords.longitude };

		var mapOptions = {
			center: currentLocation,
			zoom: 15,
			styles: ConfigService.styles
		}
		$scope.map.setOptions(mapOptions)


		var request = {
			location: currentLocation,
			//radius: 1000,
			types: ['atm'],
			rankBy: google.maps.places.RankBy.DISTANCE
		};


		service.nearbySearch(request, requestAtmCallback);
	})

	function requestAtmCallback(results, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			for (var i = 0; i < results.length; i++) {
				let atmResults = results[i];
				let latLngObject = { lat: atmResults.geometry.location.lat(), lng: atmResults.geometry.location.lng()}
				let atmLocation = new google.maps.LatLng(latLngObject.lat, latLngObject.lng);
				let myLocation = new google.maps.LatLng(currentLocation.lat, currentLocation.lng)
				let distanceAtm = google.maps.geometry.spherical.computeDistanceBetween(myLocation, atmLocation);

				console.log(results[i]);
				createMarker(results[i]);
				console.log(results[i])
				$scope.$apply(function () {

				$scope.atmList.push({ name: atmResults.name, vicinity: atmResults.vicinity,distance: distanceAtm, location: latLngObject })
				})
			}

		console.log($scope.atmList)
		}
	}

	function createMarker(place) {
		var placeLoc = place.geometry.location;
		var marker = new google.maps.Marker({
			map: $scope.map,
			position: place.geometry.location
		});
		google.maps.event.addListener(marker, 'click', function () {
			infowindow.setContent(place.name);
			infowindow.open($scope.map, this);
		});
	}

	}]);
app.service('ConfigService', function ($http) {

	var self = this;

    self.stylesStatic = 'style=element:labels.icon%7Cvisibility:off&style=element:labels.text.fill%7Ccolor:0x000000%7Csaturation:36%7Clightness:40&style=element:labels.text.stroke%7Ccolor:0x000000%7Clightness:16%7Cvisibility:on&style=feature:administrative%7Celement:geometry.fill%7Ccolor:0x000000%7Clightness:20&style=feature:administrative%7Celement:geometry.stroke%7Ccolor:0x000000%7Clightness:17%7Cweight:1.2&style=feature:landscape%7Celement:geometry%7Ccolor:0x000000%7Clightness:20&style=feature:poi%7Celement:geometry%7Ccolor:0x000000%7Clightness:21&style=feature:road.arterial%7Celement:geometry%7Ccolor:0x000000%7Clightness:18&style=feature:road.highway%7Celement:geometry.fill%7Ccolor:0x000000%7Clightness:17&style=feature:road.highway%7Celement:geometry.stroke%7Ccolor:0x000000%7Clightness:29%7Cweight:0.2&style=feature:road.local%7Celement:geometry%7Ccolor:0x000000%7Clightness:16&style=feature:transit%7Celement:geometry%7Ccolor:0x000000%7Clightness:19&style=feature:water%7Celement:geometry%7Ccolor:0x000000%7Clightness:17'

    self.styles = [
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    }
    ]
})


app.service('GetLocation', function ($http, $q, $window) {

	var self = this;

    self.locate = function(){
        var deferred = $q.defer();

        if (!$window.navigator.geolocation) {
            deferred.reject('Not suported.');
            alert("GetLocation not Working")
        } else {
            $window.navigator.geolocation.getCurrentPosition(
                function (position) {
                    //alert("Working")
                    deferred.resolve(position);
                },
                function (error) {
                    deferred.reject(error);
                    alert(error + "Nedozvoljen pristup za lokaciju")
                });
        }

        return deferred.promise;
    }
})
app.directive('staticMap', function($rootScope, ConfigService){

return { 
    templateUrl: '/js/directives/static-map/static-map.html',
    restrict: 'AE',
    scope:{ 
            mapAttr: '=',
            closeModal: '&'
          },
    link: function(scope, elem, attr){
      scope.mapAttr;

      scope.coords = {
        lat: 0,
        lng: 0
      }

      scope.googleStaticUrl = '//maps.googleapis.com/maps/api/staticmap?';
      scope.APIkey = 'key=AIzaSyD9sZ7lJltNV1y1ns3SRynHt9PId9gxc88';
      scope.size = 'size=400x400';
      scope.zoomNum = 16;
      scope.zoom = 'zoom=' + scope.zoomNum.toString();
      scope.position = scope.coords.lat.toString() + ',' + scope.coords.lng.toString();

      scope.atm = scope.mapAttr || "empty";
      
      scope.present = false;
      scope.$watch('mapAttr', function(newValue, oldValue, scope) {
        scope.present = true;
        scope.coords.lat = scope.mapAttr.location.lat || '44.739558';
        scope.coords.lng = scope.mapAttr.location.lng || '20.434156799999982';
        scope.position = scope.coords.lat.toString() + ',' + scope.coords.lng.toString();
        scope.marker = '&markers=label:default|' + scope.position;
        scope.atm = '';
        scope.atm = scope.mapAttr;
      });
      scope.style = ConfigService.stylesStatic;
    }  
}

});

