app.service('GetLocation', function ($http, $q, $window) {

	var self = this;

    self.locate = function(){
        var deferred = $q.defer();

        if (!$window.navigator.geolocation) {
            deferred.reject('Not suported.');
            alert("GetLocation not Working")
        } else {
            $window.navigator.geolocation.getCurrentPosition(
                function (position) {
                    //alert("Working")
                    deferred.resolve(position);
                },
                function (error) {
                    deferred.reject(error);
                    alert(error + "Nedozvoljen pristup za lokaciju")
                });
        }

        return deferred.promise;
    }
})