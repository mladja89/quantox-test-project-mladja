app.controller('MainCtrl', ['$scope',
	'GetLocation',
	'ConfigService',
	function ($scope, GetLocation, ConfigService) {

	$scope.propertyName = 'distance';
	$scope.reverse = true;
	$scope.sortBy = function(propertyName) {
		//console.log(propertyName)
	  $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	  $scope.propertyName = propertyName;
	};

	$scope.map = new google.maps.Map(document.getElementById('map'));
	var infowindow = new google.maps.InfoWindow();
	var service = new google.maps.places.PlacesService($scope.map);
	var directionsService = new google.maps.DirectionsService;
	$scope.atmList = [];
	var currentLocation = {};

	$scope.showModal = false;

	$scope.getAtm = function(atm) {
		$( ".map-flex-container" ).addClass('map-blur')
		$scope.showModal = true;
	    console.log(atm)
		$scope.mapAttr = atm;
	}

	$scope.closeModal = function (a) {
		$scope.showModal = false;
		$( ".map-flex-container" ).removeClass('map-blur')
	}



	GetLocation.locate().then(function (a) {

		console.log(a)

		currentLocation =  { lat: a.coords.latitude, lng: a.coords.longitude };

		var mapOptions = {
			center: currentLocation,
			zoom: 15,
			styles: ConfigService.styles
		}
		$scope.map.setOptions(mapOptions)


		var request = {
			location: currentLocation,
			//radius: 1000,
			types: ['atm'],
			rankBy: google.maps.places.RankBy.DISTANCE
		};


		service.nearbySearch(request, requestAtmCallback);
	})

	function requestAtmCallback(results, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			for (var i = 0; i < results.length; i++) {
				let atmResults = results[i];
				let latLngObject = { lat: atmResults.geometry.location.lat(), lng: atmResults.geometry.location.lng()}
				let atmLocation = new google.maps.LatLng(latLngObject.lat, latLngObject.lng);
				let myLocation = new google.maps.LatLng(currentLocation.lat, currentLocation.lng)
				let distanceAtm = google.maps.geometry.spherical.computeDistanceBetween(myLocation, atmLocation);

				console.log(results[i]);
				createMarker(results[i]);
				console.log(results[i])
				$scope.$apply(function () {

				$scope.atmList.push({ name: atmResults.name, vicinity: atmResults.vicinity,distance: distanceAtm, location: latLngObject })
				})
			}

		console.log($scope.atmList)
		}
	}

	function createMarker(place) {
		var placeLoc = place.geometry.location;
		var marker = new google.maps.Marker({
			map: $scope.map,
			position: place.geometry.location
		});
		google.maps.event.addListener(marker, 'click', function () {
			infowindow.setContent(place.name);
			infowindow.open($scope.map, this);
		});
	}

	}]);