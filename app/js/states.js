// Application Level State
app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.when('', '/main');

  $stateProvider
    .state('app', {
      url: '',
      controller: 'AppCtrl',
      views: {
        'controls': {
          templateUrl: 'js/templates/controls.html',
          controller: 'ControlsCtrl'
        },
        'main': {
          templateUrl: 'js/templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })
    .state('app.main', {
      url: '/main',
      templateUrl: 'js/templates/main.html',
      controller: 'MainCtrl'
    })
    .state('404', {
      url: '/404',
      templateUrl: 'js/templates/404.html',
      controller: 'AppCtrl'
    });

}]);