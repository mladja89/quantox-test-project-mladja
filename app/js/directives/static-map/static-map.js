app.directive('staticMap', function($rootScope, ConfigService){

return { 
    templateUrl: '/js/directives/static-map/static-map.html',
    restrict: 'AE',
    scope:{ 
            mapAttr: '=',
            closeModal: '&'
          },
    link: function(scope, elem, attr){
      scope.mapAttr;

      scope.coords = {
        lat: 0,
        lng: 0
      }

      scope.googleStaticUrl = '//maps.googleapis.com/maps/api/staticmap?';
      scope.APIkey = 'key=AIzaSyD9sZ7lJltNV1y1ns3SRynHt9PId9gxc88';
      scope.size = 'size=400x400';
      scope.zoomNum = 16;
      scope.zoom = 'zoom=' + scope.zoomNum.toString();
      scope.position = scope.coords.lat.toString() + ',' + scope.coords.lng.toString();

      scope.atm = scope.mapAttr || "empty";
      
      scope.present = false;
      scope.$watch('mapAttr', function(newValue, oldValue, scope) {
        scope.present = true;
        scope.coords.lat = scope.mapAttr.location.lat || '44.739558';
        scope.coords.lng = scope.mapAttr.location.lng || '20.434156799999982';
        scope.position = scope.coords.lat.toString() + ',' + scope.coords.lng.toString();
        scope.marker = '&markers=label:default|' + scope.position;
        scope.atm = '';
        scope.atm = scope.mapAttr;
      });
      scope.style = ConfigService.stylesStatic;
    }  
}

});

