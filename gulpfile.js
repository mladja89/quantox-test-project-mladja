var gulp = require('gulp');
var watch = require('gulp-watch');
var concat = require('gulp-concat');
gulp.task('default', ['watch:concat']);
//gulp.task('mergeScripts', function(){
//	gulp.src('js/**/*.js').pipe(concat('vendor.js')).pipe(gulp.dest('js'))
//
//})

gulp.task('watch:concat', function (cb) {
	concatTask();
	watch('app/js/**/*.js', {name: 'Gulp', verbose: true}, concatTask);
	watch('app/css/**/*.css', {name: 'Gulp', verbose: true}, concatTask);
});


gulp.task('concat', concatTask);

function concatTask() { 
	gulp.src('app/js/**/*.js')
		.pipe(concat('vendor.js')) 
		.pipe(gulp.dest('app/vendor'));
	gulp.src('app/css/**/*.css')
		.pipe(concat('vendor.css'))
		.pipe(gulp.dest('app/vendor'));
}